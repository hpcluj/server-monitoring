#!/usr/bin/env python

import os
import sys
import time
import uuid
import socket
import argparse

import pylibinfo
import report


def read_conf(path):
    try:
        with open(path, 'r') as f:
            return f.read().strip()
    except IOError:
        return None


def write_conf(path, mid):
    with open(path, 'w') as f:
        f.write(str(mid))


def configure_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--configfile', help='Use custom configuration file',
                        default=os.path.expanduser(os.path.join('~', '.codewarsagent')))
    parser.add_argument('-i', '--interval', type=float, default=1.0,
                        help='Interval between sending reports, in seconds (default 1)')
    parser.add_argument('-n', '--name', default=socket.getfqdn(),
                        help='If registering, use NAME instead of the system\'s hostname')
    parser.add_argument('-r', '--register', action='store_true',
                        help='Force re-registration of this machine')
    parser.add_argument('server', help='Address:port of the server where to report')
    return parser


def main():
    args = configure_parser().parse_args()
    svr, port = args.server.split(':')
    port = int(port)
    mid = read_conf(args.configfile)
    if mid is None or args.register:
        mid = str(uuid.uuid1())
        reporter = report.HTTPReporter(mid, svr, port)
        reporter.register_machine(args.name)
        write_conf(args.configfile, mid)
    else:
        reporter = report.HTTPReporter(mid, svr, port)
    linfo = pylibinfo.Libinfo()
    while True:
        totalram, freeram = map(int, linfo.get_raminfo())
        info = {
            'memoryTotalSize': totalram,
            'memoryFreeSpace': freeram,
            'diskFreeSpace': linfo.get_system_freespace(),
            'processesNumber': int(linfo.get_procinfo()[0])
        }
        reporter.send_report(info)
        time.sleep(args.interval)


if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        print
        print 'Shutting down...'
