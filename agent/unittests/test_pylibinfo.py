#!/usr/bin/env python

import unittest

import pylibinfo

class TestPyLibinfo(unittest.TestCase):

    def test(self):
        testee = pylibinfo.Libinfo()
        self.assertNotEqual(testee.get_raminfo(), 0)
        self.assertNotEqual(testee.get_procinfo(), 0)
        self.assertGreaterEqual(testee.get_system_freespace, 0)


if __name__ == '__main__':
    unittest.main(module='test_pylibinfo')
