#include <Windows.h>
#include <Psapi.h>

#include "process.h"

int get_win32_procinfo (struct win32_procinfo *proc)
{
	DWORD *pids, bufsize, retBytes;
	BOOL ret;
	bufsize = sizeof(DWORD) << 10;
	pids = malloc(bufsize);
	ret = EnumProcesses(pids, bufsize, &retBytes);
	if (ret)
	{
		proc->procsno = (unsigned short)(retBytes/sizeof(DWORD));
	}
	free(pids);
	return !((int)ret);
}