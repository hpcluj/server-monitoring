/*
 * ram.h
 */

#ifndef _WIN32RAM_H
#define _WIN32RAM_H

struct win32_ram {
	unsigned long totalram;
	unsigned long freeram;
	unsigned long totalpagefile;
	unsigned long freepagefile;
};

int get_win32_ram (struct win32_ram *ram);

#endif