#include <sys/statvfs.h>

#include "diskspace.h"

long get_root_freespace (void)
{
    struct statvfs buf;
    if (!statvfs("/", &buf))
    {
        return (buf.f_bsize * buf.f_bfree);
    }
    else
    {
        return -1;
    }
}
