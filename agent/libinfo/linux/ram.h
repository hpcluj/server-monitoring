/*
 * ram.h
 */

#ifndef _LINUXRAM_H
#define _LINUXRAM_H

struct linux_ram {
    unsigned long totalram;
    unsigned long freeram;
    unsigned long sharedram;
    unsigned long bufferram;
    unsigned long totalswap;
    unsigned long freeswap;
};

int get_linux_ram (struct linux_ram *ram);

#endif
