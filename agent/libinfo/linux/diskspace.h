/*
 * diskspace.h
 */

#ifndef _LINUXDISKSPACE_H
#define _LINUXDISKSPACE_H

long get_root_freespace (void);

#endif
