#include <sys/sysinfo.h>

#include "process.h"

int get_linux_procinfo (struct linux_procinfo * lproc)
{
    struct sysinfo info;
    if (!sysinfo(&info))
    {
        lproc->procsno = info.procs;
        return 0;
    }
    else
    {
        return -1;
    }
}
