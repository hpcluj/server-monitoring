#include <stdlib.h>

#ifdef __linux__
#include "linux/ram.h"
#include "linux/process.h"
#include "linux/diskspace.h"
#endif

#ifdef _WIN32
#include "windows/ram.h"
#include "windows/process.h"
#include "windows/diskspace.h"
#endif

#include "info.h"

/* Linux-specific code */
#ifdef __linux__

int get_raminfo (struct raminfo *info)
{
	struct linux_ram lram;
	int ret = get_linux_ram(&lram);
	if (!ret)
	{
		info->totalram = lram.totalram;
		info->freeram = lram.freeram;
	}
	return ret;
}

int get_procinfo (struct procinfo *info)
{
	struct linux_procinfo lproc;
	int ret = get_linux_procinfo(&lproc);
	if (!ret)
	{
		info->processcount = lproc.procsno;
	}
	return ret;
}

long get_system_freespace (void)
{
	return (get_root_freespace());
}

#endif

/* Windows-specific code */
#ifdef _WIN32

int get_raminfo (struct raminfo *info)
{
	struct win32_ram wram;
	int ret;
	ret = get_win32_ram(&wram);
	if (!ret)
	{
		info->totalram = wram.totalram;
		info->freeram = wram.freeram;
	}
	return ret;
}

int get_procinfo (struct procinfo *info)
{
	struct win32_procinfo wproc;
	int ret;
	ret = get_win32_procinfo(&wproc);
	if (!ret)
	{
		info->processcount = wproc.procsno;
	}
	return ret;
}

long get_system_freespace (void)
{
	return get_root_freespace();
}

#endif
