/*
 * info.h
 */

#ifndef _INFO_H
#define _INFO_H

struct raminfo {
    unsigned long totalram;
    unsigned long freeram;
};

struct procinfo {
    unsigned short processcount;
};

int get_raminfo (struct raminfo *info);
int get_procinfo (struct procinfo *info);
long get_system_freespace (void);

#endif
