#!/usr/bin/env python

"""Module offering reporting functionality."""

import json
import httplib


class HTTPReporter(object):

    def __init__(self, machine_id, host, port=80):
        self.machine_id = machine_id
        self.host = host
        self.port = port
        self.heads = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def send_report(self, data):
        url = '/server-monitoring/api/servers/{!s}/detail'.format(self.machine_id)
        payload = json.dumps(data)
        conn = httplib.HTTPConnection(self.host, self.port)
        try:
            conn.request('PUT', url, body=payload, headers=self.heads)
            if conn.getresponse().status not in (200, 202):
                raise Exception('Sending report failed!')
        finally:
            conn.close()

    def register_machine(self, machine_name):
        url = '/server-monitoring/api/servers'
        payload = json.dumps({
            'id': str(self.machine_id),
            'name': machine_name
        })
        conn = httplib.HTTPConnection(self.host, self.port)
        try:
            conn.request('POST', url, body=payload, headers=self.heads)
            if conn.getresponse().status != 201:
                raise Exception('Registering machine failed!')
        finally:
            conn.close()
