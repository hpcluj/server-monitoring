package com.hp.tools.monitoring.entities;

import com.hp.tools.monitoring.util.DateTimeAdapter;
import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
@XmlRootElement
public class ServerDetails {
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime timestamp;
    private long diskFreeSpace;
    private int processesNumber;
    private long memoryFreeSpace;
    private long memoryTotalSize;

    /**
     * Used by JAXB.
     */
    public ServerDetails() {
    }

    public ServerDetails(DateTime timestamp, long diskFreeSpace, int processesNumber, long memoryFreeSpace, long memoryTotalSize) {
        this.timestamp = timestamp;
        this.diskFreeSpace = diskFreeSpace;
        this.processesNumber = processesNumber;
        this.memoryFreeSpace = memoryFreeSpace;
        this.memoryTotalSize = memoryTotalSize;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public long getDiskFreeSpace() {
        return diskFreeSpace;
    }

    public void setDiskFreeSpace(long diskFreeSpace) {
        this.diskFreeSpace = diskFreeSpace;
    }

    public int getProcessesNumber() {
        return processesNumber;
    }

    public void setProcessesNumber(int processesNumber) {
        this.processesNumber = processesNumber;
    }

    public long getMemoryFreeSpace() {
        return memoryFreeSpace;
    }

    public void setMemoryFreeSpace(long memoryFreeSpace) {
        this.memoryFreeSpace = memoryFreeSpace;
    }

    public long getMemoryTotalSize() {
        return memoryTotalSize;
    }

    public void setMemoryTotalSize(long memoryTotalSize) {
        this.memoryTotalSize = memoryTotalSize;
    }
}
