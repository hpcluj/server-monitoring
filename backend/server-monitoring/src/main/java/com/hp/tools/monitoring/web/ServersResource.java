package com.hp.tools.monitoring.web;

import com.hp.tools.monitoring.entities.Server;
import com.hp.tools.monitoring.entities.ServerDetails;
import com.hp.tools.monitoring.entities.ServerHistory;
import com.hp.tools.monitoring.services.MonitoringService;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
@Singleton
@Path("/servers")
public class ServersResource {
    private MonitoringService service;

    public ServersResource() {
        service = new MonitoringService();
    }

    @GET
    @Path("/{serverId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Server getServer(@PathParam("serverId") String serverId) {
        return service.getServer(serverId);
    }

    @GET
    @Path("/{serverId}/history")
    @Produces({MediaType.APPLICATION_JSON})
    public List<ServerDetails> getServerHistory(@PathParam("serverId") String serverId) {
        return service.getServerHistory(serverId);
    }

    @GET
    @Path("/history")
    @Produces({MediaType.APPLICATION_JSON})
    public List<ServerHistory> getHistories() {
        return service.getHistories();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response postServer(Server server) {
        boolean created = service.addServer(server);
        if (created) {
            return Response.created(URI.create("servers/" + server.getId())).build();
        }

        return Response.status(409).build(); //Conflict
    }

    @PUT
    @Path("/{serverId}/detail")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response putServerDetails(@PathParam("serverId") String serverId, ServerDetails serverDetails) {
        boolean updated = service.updateServerDetails(serverId, serverDetails);
        if (updated) {
            return Response.accepted(serverDetails).build();
        }

        return Response.status(404).build(); //Not Found
    }
}
