package com.hp.tools.monitoring.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author Octavian
 * @since 1.0-SNAPSHOT
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

    public DateTime unmarshal(String v) throws Exception {
        DateTimeFormatter fmt = ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC);
        return fmt.parseDateTime(v);
    }

    public String marshal(DateTime v) throws Exception {
        DateTimeFormatter fmt = ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC);
        return fmt.print(v);
    }
}
