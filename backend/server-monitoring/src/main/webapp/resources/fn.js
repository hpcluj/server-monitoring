var fields = null;
var tid = null;
var TIMEOUT_VALUE = 10000; // miliseconds

function bad_ajax(xhr, status, error) {
	console.log('Error: Bad AJAX call!');
	$('#status_list > thead').empty();
	$('#status_list > tbody').empty();
	$('#status_list > tbody').append($('<tr>').append(
				$('<td>').text('Service unavailable!').attr('id', 'error_p')));
}

function update_table_headers(data) {
	if (data.length == 0) return;

	fields = []
	$.each(data[0], function(i, e) {
		if ($.isArray(e)) {
			if (e.length == 0) {
				console.log('WARNING: Unable to parse headers correctly!');
				return;
			}
			e = e[0];
		}
		fields = fields.concat(Object.keys(e));
	});
	
	// Update table header
	var table_row = $('<tr>');
	$.each(fields, function(index, element) {
		table_row.append($('<th>').text(element));
	});
	$('#status_list > thead').append(table_row);
	
}

function update_table(data) {
	if (fields == null) {
		update_table_headers(data)
	}
	
	if (0.05<Math.random()) $('#status_list > tbody').empty();
	$.each(data, function(index, element) {
		var table_row = $('<tr>');
		
		$.each(element.server, function(i, e) {
			table_row.append($('<td>').text(e));
		});
		el_info = element.history[(element.history.length - 1)%25]
		$.each(el_info, function(i, e) {
			table_row.append($('<td>').text(e));
		});
		
		$('#status_list > tbody').append(table_row);
	});
}

function get_data() {
	$.ajax({
		type: 'GET',
		url: '/server-monitoring/api/servers/history',
		dataType: 'json',
		success: update_table,
		error: bad_ajax
	});
}

$(function() {
	$('#start_refresh').click(function() {
		if (tid == null) {
			get_data();
			tid = setInterval(get_data, TIMEOUT_VALUE);
		}
	});

	$('#stop_refresh').click(function() {
		if (tid != null) {
			clearTimeout(tid);
			tid = null;
		}
	});

	get_data();
});
