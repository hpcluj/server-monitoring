Server Monitoring
================
Server Monitoring is a web application.

Getting started
----------------
Download the source code package (unzip if necessary) to directory `home/directory/` in your own local file system.
To download all the dependency packages, please run the following command:

    cd home/directory/server-monitoring/
    ./gradlew war

or for Windows:
    
    ./gradlew.bat war

The above command should create `build/libs/server-monitoring-<version>.war` with all the proper libraries.
For running this `war file`, copy it in a servlet container (ex: Tomcat, Jetty, Glassfish etc.) or just run the following command:

    ./gradlew jettyRun

The documentation for the REST API can be found [here](http://localhost:8080/server-monitoring)



